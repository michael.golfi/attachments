# Attachments

## Description

This project is based somewhat on the article [Handling 1 Million Requests per Minute with Go](http://marcio.io/2015/07/handling-1-million-requests-per-minute-with-golang/).

The goal of this project is to efficiently concurrently upload files which could potentially reach a Gigabyte in size. These files could be log files and other data.

## Architecture

This is intended to run in a pod with a mounted NFS. The attachments service will save to its local file system as normal while the NFS will be abstracted to the pod specification level. The service will be unaware of the storage location, it will only save to a path. The NFS will need to be mounted to the path required and will assume that the directory already exists as it will be mounted at start up time.

![Architecture](/docs/assets/arch.png)

## Testing

1. Create the base folder for storing files
2. Set the path in the config.yaml file.
3. `go run main.go`
4. Execute the following test commands:

### Save Attachment(s)
```bash
curl -X POST \
  http://localhost:8080/attachments/{{ticketId}} \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -F files=@file1.txt \
  -F file2=@file2.txt

```

### List Attachments
```bash
curl -X GET http://localhost:8080/attachments/{{ticketId}}
```

### Get Attachment
```bash
curl -X GET http://localhost:8080/attachments/{{ticketId}}/{{attachmentName}}
```

### Delete Attachment
```bash
curl -X DELETE http://localhost:8080/attachments/{{ticketId}}/{{attachmentName}}
```