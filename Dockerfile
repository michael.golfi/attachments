FROM scratch
MAINTAINER Michael Golfi <michael.m.golfi@gmail.com>

ADD ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
ADD config.yaml /config.yaml

ARG SERVICE_NAME
ADD $SERVICE_NAME /

CMD [ "/$SERVICE_NAME" ]
EXPOSE 8080