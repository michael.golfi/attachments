package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/michael.golfi/attachments/storage"

	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

var (
	name = "attachments"
	port = ":8080"
)

func setupViper() {
	viper.AddConfigPath(".")
	viper.AddConfigPath("/root")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Fatal error config file: %s \n", err.Error())
	}
}

func main() {
	setupViper()

	router := mux.NewRouter().
		PathPrefix(fmt.Sprintf("/%s", name)).
		Subrouter()

	setupHandlers(router)

	log.Printf("Started %s at %s", name, port)
	log.Fatal(http.ListenAndServe(port, router))
}

func setupHandlers(r *mux.Router) {
	handler := &storage.AttachmentHandler{
		CanDelete: viper.GetBool("attachments.canDelete"),
		BasePath:  viper.GetString("attachments.mountPath"),
	}

	handler.SetRoutes(r)
}
