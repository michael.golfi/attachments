package storage

import (
	"encoding/json"
	"io"
	"net/http"
	"os"
	"path"
	"path/filepath"

	"io/ioutil"

	"github.com/gorilla/mux"
)

// AttachmentHandler handles all actions pertaining to attachment management
// and whether they can be deleted and the base path for storing attachments.
type AttachmentHandler struct {
	CanDelete bool
	BasePath  string
}

// ListAttachment lists all the files belonging in a directory of a $ticketId
func (a *AttachmentHandler) ListAttachment() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		ticketID := mux.Vars(r)["ticketId"]
		info, err := ioutil.ReadDir(path.Join(a.BasePath, ticketID))

		if err != nil {
			http.Error(w, "Ticket does not exist", http.StatusInternalServerError)
			return
		}

		fileHeaders := make([]FileHeader, len(info))
		for i, header := range info {

			fileHeaders[i] = FileHeader{
				Name:       header.Name(),
				LastUpdate: header.ModTime(),
				Size:       header.Size(),
			}

		}

		if err := json.NewEncoder(w).Encode(&fileHeaders); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

// GetAttachment will download an attachment belonging to a ticket
func (a *AttachmentHandler) GetAttachment() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)

		ticketID := vars["ticketId"]
		attachmentName := vars["attachmentName"]

		http.ServeFile(w, r, path.Join(a.BasePath, ticketID, attachmentName))
	}
}

// SaveAttachment saves one or many files according to a multipart-file upload
func (a *AttachmentHandler) SaveAttachment() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := r.ParseMultipartForm(32 << 20); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		ticketID := mux.Vars(r)["ticketId"]

		if ok, err := exists(path.Join(a.BasePath, ticketID)); !ok && err == nil {
			os.Mkdir(path.Join(a.BasePath, ticketID), os.ModePerm)
		} else if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		for _, fileHeaders := range r.MultipartForm.File {
			for i := range fileHeaders {
				file, err := fileHeaders[i].Open()
				defer file.Close()

				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				strippedFileName := filepath.Base(fileHeaders[i].Filename)
				fileName := path.Join(a.BasePath, ticketID, strippedFileName)
				dest, err := os.Create(fileName)
				defer dest.Close()

				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				if _, err := io.Copy(dest, file); err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
			}
		}
	}
}

// DeleteAttachment will handle deleting attachments belonging to a ticket
func (a *AttachmentHandler) DeleteAttachment() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)

		ticketID := vars["ticketId"]
		attachmentName := vars["attachmentName"]

		if err := os.Remove(path.Join(a.BasePath, ticketID, attachmentName)); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}
