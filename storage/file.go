package storage

import (
	"os"
	"time"
)

type FileHeader struct {
	Name       string    `json:"name"`
	LastUpdate time.Time `json:"lastUpdate"`
	Size       int64     `json:"size"`
}

func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}
