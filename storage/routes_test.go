package storage_test

import (
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
	"gitlab.com/michael.golfi/attachments/storage"
)

func Test_Routes(t *testing.T) {
	router := mux.NewRouter()

	handler := storage.AttachmentHandler{
		CanDelete: true,
	}

	handler.SetRoutes(router)

	require.NotNil(t, router.GetRoute("ListAttachment"))
	require.NotNil(t, router.GetRoute("GetAttachment"))
	require.NotNil(t, router.GetRoute("SaveAttachment"))
	require.NotNil(t, router.GetRoute("DeleteAttachment"))
}
