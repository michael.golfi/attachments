package storage_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"os"
	"testing"

	"gitlab.com/michael.golfi/attachments/storage"

	"net/http"
	"net/http/httptest"

	"time"

	"path/filepath"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
)

var (
	fileContent = "hello\ngo\n"
)

func Test_SaveAttachment_Successful(t *testing.T) {
	os.Mkdir("../SaveAttachment", os.ModePerm)

	req, err := postFile("../config.yaml", "http://localhost:8080/attachments/123456")
	require.NoError(t, err)

	setupRouter(true, "../SaveAttachment", req)

	ok, err := exists("../SaveAttachment/123456/config.yaml")
	require.NoError(t, err)
	require.True(t, ok)
	os.RemoveAll("../SaveAttachment")
}

func Benchmark_SaveAttachment_ManyFiles(b *testing.B) {
	os.Mkdir("../ManyFiles", os.ModePerm)

	f, _ := os.Create("../ManyFiles/file.txt")
	f.Truncate(1e7)

	router := mux.NewRouter().
		PathPrefix("/attachments").
		Subrouter()

	handler := storage.AttachmentHandler{
		BasePath: "../ManyFiles",
	}

	handler.SetRoutes(router)
	go http.ListenAndServe(":8080", router)

	req, _ := postFile("../ManyFiles/file.txt", "http://localhost:8080/attachments/123456")
	client := http.Client{}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		client.Do(req)
	}

	b.StopTimer()
	os.RemoveAll("../ManyFiles")
}

func Benchmark_SaveAttachment_LargeFile(b *testing.B) {
	os.Mkdir("../LargeFiles", os.ModePerm)

	f, _ := os.Create("../LargeFiles/file.txt")
	f.Truncate(1e9)

	router := mux.NewRouter().
		PathPrefix("/attachments").
		Subrouter()

	handler := storage.AttachmentHandler{
		BasePath: "../LargeFiles",
	}

	handler.SetRoutes(router)
	go http.ListenAndServe(":8080", router)

	req, _ := postFile("../LargeFiles/file.txt", "http://localhost:8080/attachments/123456")
	client := http.Client{}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		client.Do(req)
	}

	b.StopTimer()
	os.RemoveAll("../LargeFiles")
}

func Test_ListAttachment_Successful(t *testing.T) {
	update := time.Now().Truncate(time.Second)
	createFile("../ListAttachment/123456/file.txt", fileContent)
	createFile("../ListAttachment/123456/file2.txt", fileContent)

	req := httptest.NewRequest("GET", "http://localhost:8080/attachments/123456", nil)
	w := setupRouter(true, "../ListAttachment", req)

	var files []storage.FileHeader
	err := json.NewDecoder(w.Body).Decode(&files)
	require.NoError(t, err)
	require.Len(t, files, 2)
	require.EqualValues(t, len(fileContent), files[0].Size)
	require.EqualValues(t, len(fileContent), files[1].Size)
	equalDates(t, update, files[0].LastUpdate)
	equalDates(t, update, files[1].LastUpdate)
	os.RemoveAll("../ListAttachment")
}

func Test_GetAttachment_Successful(t *testing.T) {
	createFile("../GetAttachment/123456/file.txt", fileContent)
	req := httptest.NewRequest("GET", "http://localhost:8080/attachments/123456/file.txt", nil)
	w := setupRouter(true, "../GetAttachment", req)

	bytes, err := ioutil.ReadAll(w.Body)
	require.Equal(t, fileContent, string(bytes))
	require.NoError(t, err)
	os.RemoveAll("../GetAttachment")
}

func Test_DeleteAttachment_Successful(t *testing.T) {
	createFile("../DeleteAttachment/123456/file.txt", fileContent)

	req := httptest.NewRequest("DELETE", "http://localhost:8080/attachments/123456/file.txt", nil)
	setupRouter(true, "../DeleteAttachment", req)

	fileExists, err := exists("../DeleteAttachment/123456/file.txt")
	require.False(t, fileExists)
	require.NoError(t, err)
	os.RemoveAll("../DeleteAttachment")
}

func setupRouter(canDelete bool, basePath string, req *http.Request) *httptest.ResponseRecorder {
	router := mux.NewRouter().
		PathPrefix("/attachments").
		Subrouter()

	handler := storage.AttachmentHandler{
		CanDelete: canDelete,
		BasePath:  basePath,
	}

	w := httptest.NewRecorder()
	handler.SetRoutes(router)
	router.ServeHTTP(w, req)
	return w
}

func postFile(filename string, targetUrl string) (*http.Request, error) {
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)

	fileWriter, err := bodyWriter.CreateFormFile("uploadfile", filename)
	if err != nil {
		fmt.Println("error writing to buffer")
		return nil, err
	}

	fh, err := os.Open(filename)
	if err != nil {
		fmt.Println("error opening file")
		return nil, err
	}

	_, err = io.Copy(fileWriter, fh)
	if err != nil {
		return nil, err
	}

	contentType := bodyWriter.FormDataContentType()
	bodyWriter.Close()

	req, err := http.NewRequest("POST", targetUrl, bodyBuf)
	req.Header.Set("Content-Type", contentType)
	return req, err
}

func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

func createFile(path, content string) {
	prefix := filepath.Dir(path)
	os.MkdirAll(prefix, os.ModePerm)
	ioutil.WriteFile(path, []byte(content), os.ModePerm)
}

func equalDates(t *testing.T, expected, actual time.Time) {
	require.Equal(t, expected.Truncate(time.Second), actual.Truncate(time.Second))
}
