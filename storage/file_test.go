package storage

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_exists_Successful(t *testing.T) {
	ok, err := exists("../config.yaml")
	require.True(t, ok)
	require.NoError(t, err)
}

func Test_exists_DoesNotExist(t *testing.T) {
	ok, err := exists("../dne.yaml")
	require.False(t, ok)
	require.Nil(t, err)
}

func Test_exists_BadPath(t *testing.T) {
	ok, err := exists("~+=^%$#!/sfsf/config.yaml")
	require.False(t, ok)
	require.Nil(t, err)
}
