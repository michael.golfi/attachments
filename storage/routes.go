package storage

import "github.com/gorilla/mux"

// SetRoutes sets up the routes for handling attachment
func (a *AttachmentHandler) SetRoutes(r *mux.Router) {
	r.HandleFunc("/{ticketId}", a.ListAttachment()).
		Methods("GET").
		Name("ListAttachment")

	r.HandleFunc("/{ticketId}/{attachmentName}", a.GetAttachment()).
		Methods("GET").
		Name("GetAttachment")

	r.HandleFunc("/{ticketId}", a.SaveAttachment()).
		Methods("POST", "PUT").
		Name("SaveAttachment")

	if a.CanDelete {
		r.HandleFunc("/{ticketId}/{attachmentName}", a.DeleteAttachment()).
			Methods("DELETE").
			Name("DeleteAttachment")
	}
}
